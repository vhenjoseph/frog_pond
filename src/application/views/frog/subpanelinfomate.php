<!-- Subpanel Frog Mate Info -->
<div class="alert alert-info">
	<?php echo $detail['first_name'].' '.$detail['last_name']?> is in relationship with <a href="<?php echo base_url("frog/detail/".$mate_info['id']);?>" class="alert-link"><?php echo $mate_info['first_name'].' '.$mate_info['last_name']?></a>.
</div>