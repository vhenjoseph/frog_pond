<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Frog <?php echo $detail['first_name'].' '.$detail['last_name'] ?> </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" id="form_edit" method="post" action="<?php echo base_url("frog/process");?>">
						<div class="form-group">
							<input type="hidden" name="id" value="<?php echo $detail['id'];?>"/>
							<label>First Name <span class="text-danger">*</span></label>
							<input required="required" class="form-control" placeholder="First Name" name="first_name" value="<?php echo html_escape($detail['first_name']);?>"/>
							<?php echo form_error('first_name'); ?>
							<br/>
							<label>Last Name <span class="text-danger">*</span></label>
							<input required="required" class="form-control" placeholder="Last Name" name="last_name" value="<?php echo html_escape($detail['last_name']);?>"/>
							<?php echo form_error('last_name'); ?>
							<br/>
							<label>Gender <span class="text-danger">*</span></label>
							<br/>
							<div required="required" class="btn-group" data-toggle="buttons">
								<label id="genderLabelM" class="btn btn-gender btn-default">
									<input required="required" class="btn btn-gender btn-default" type="radio" id="genderM" name="gender" value="M"> Male
								</label>
								<label id="genderLabelF" class="btn btn-gender btn-default">
									<input required="required" class="btn btn-gender btn-default" type="radio" id="genderF" name="gender" value="F"> Female
								</label>
							</div>
							<br/>
							<?php echo form_error('gender'); ?>
							<br/>
							<label>Birth Date <span class="text-danger">*</span></label>
							<div id="birth_date" class="input-group date">
							  <input required="required" class="form-control" placeholder="mm/dd/yyyy" type="text" name="birth_date" value="<?php echo html_escape($detail['birth_date']);?>"/>
							  <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
							</div>
							<?php echo form_error('birth_date'); ?>
							<br/>
							<label>Pond <span class="text-danger">*</span></label>
							<?php
								$attrib = 'required="required" class="form-control"';
								echo form_dropdown('pond_list', $pond_list, $detail['pond_id'], $attrib);
							?>
							<?php echo form_error('birth_date'); ?>
							<br/>

						</div>
						<?php $this->load->view("common/buttons/submit");?>
						<?php $this->load->view("common/buttons/cancel");?>
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>
<script>
$(document).ready(function() {
	$('#birth_date').datepicker({
		todayBtn: "linked"
		,endDate: "today"
	});	
	
	$('#gender<?php echo $detail['gender']?>').addClass("active");	
	$('#gender<?php echo $detail['gender']?>').prop("checked", true);	
	$('#genderLabel<?php echo $detail['gender']?>').addClass("active");
});
</script>