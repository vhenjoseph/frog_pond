<!-- Subpanel Frog List -->
<div class="panel panel-default">
	<div class="panel-heading">
		Potential mates for <?php echo $detail['first_name'].' '.$detail['last_name']?> from Frogs pond
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<table class="table table-striped table-bordered table-hover" id="frogs_subpanel">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Gender</th>
						<th>Birth Date</th>
						<th></th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.table-responsive -->
	</div>
	<!-- /.panel-body -->
</div>
<script>
$(document).ready(function() {
	$('#frogs_subpanel').DataTable({
		"responsive": true,
		"processing": true,
        "serverSide": true,
		"ajax": {
            "url": "<?php echo base_url("frog/get_potential_mates/".$detail['id']);?>",
            "type": "POST",
			"dataSrc": function ( json ) {
				for ( var i=0, ien=json.data.length ; i<ien ; i++ ) {
					json.data[i]['first_name'] = '<a href="<?php echo base_url("frog/detail")?>/'+json.data[i]['id']+'">'+json.data[i]['first_name']+'</a>';
				}
				return json.data;
			}
        },
		"columns": [
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "gender" },
            { "data": "birth_date" }
        ],
		"columnDefs": [
			{
				"targets": [ 2 ],
				"orderable": false
			},
			{
				"targets": 4,
				"data": null,
				"render": function ( data, type, full ) {
					var button = '<a href="<?php echo base_url("mate/add/".$detail['id']);?>/'+data['id']+'" id="mate" type="button" class="btn btn-outline btn-danger btn-xs"><i class="fa fa-heart fa-fw"></i>  Select as partner</a>';
					return button;
				},
				"width": "10%"
			}
		]
	});
});


</script>