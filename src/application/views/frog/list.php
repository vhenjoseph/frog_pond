<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Frogs</h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Frog List</div>
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="frogs_list">
							<thead>
								<tr>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Gender</th>
									<th>Birth Date</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#frogs_list').DataTable({
		"responsive": true,
		"processing": true,
        "serverSide": true,
		"stateSave": true,
		"ajax": {
            "url": "<?php echo base_url("frog/getlist");?>",
            "type": "POST",
			"dataSrc": function ( json ) {
				for ( var i=0, ien=json.data.length ; i<ien ; i++ ) {
					json.data[i]['first_name'] = '<a href="<?php echo base_url("frog/detail")?>/'+json.data[i]['id']+'">'+json.data[i]['first_name']+'</a>';
					json.data[i]['last_name'] = '<a href="<?php echo base_url("frog/detail")?>/'+json.data[i]['id']+'">'+json.data[i]['last_name']+'</a>';
				}
				return json.data;
			}
        },
		"columns": [
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "gender" },
            { "data": "birth_date" }
        ],
		"columnDefs": [{
			"targets": [ 2 ],
			"orderable": false
		}]
	});
});
</script>