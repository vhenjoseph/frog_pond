<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $detail['first_name'].' '.$detail['last_name'] ?></h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php 
						if( $detail['deleted'] == 0 ) {
							$this->load->view("common/buttons/edit", array("edit_link" => "frog/edit/".$detail['id']));
							$this->load->view("common/buttons/delete", array("delete_link" => "frog/delete/".$detail['id']));
						} else {
							echo "<span class='text-danger'>We express our deepest sympathy. <i class='fa fa-frown-o fa-fw'></i></span>";
						}
					?>
				</div>
				<div class="panel-body">
					<dl class="dl-horizontal">
						<dt>First Name</dt>
						<dd><?php echo $detail['first_name']?></dd>

						<dt>Last Name</dt>
						<dd><?php echo $detail['last_name']?></dd>
						
						<dt>Gender</dt>
						<dd><?php echo $detail['gender']?></dd>
						
						<dt>Birth Date</dt>
						<dd><?php echo $detail['birth_date']?></dd>
						
						<?php
							if($detail['deleted'] == 1){
								?>
								<dt>Death Date</dt>
								<dd><?php echo $detail['death_date']?></dd>
								<?php
							}
						?>
						
						<dt>Pond</dt>
						<dd><?php echo anchor("pond/detail/".$detail['pond_id'], $detail['pond']); ?></dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<?php
		if($mate_info) {
			?>
			<div class="col-lg-12">
				<?php $this->load->view("frog/subpanelinfomate", array("detail" => $detail, "mate_info"=>$mate_info) );?>
			</div>
			<?php 
		} else {
			if( $detail['pond_id'] == 3 && $detail['deleted'] == 0 ) {
				?>
				<div class="col-lg-12">
					<?php $this->load->view("frog/subpanellistmates", array("detail" => $detail) );?>
				</div>
				<?php 
			}
		} 
	?>
	</div>
</div>
