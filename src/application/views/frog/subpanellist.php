<!-- Subpanel Frog List -->
<div class="panel panel-default">
	<div class="panel-heading">
		Frogs living in <?php echo $detail['name'] ?> pond 
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<div class="dataTable_wrapper">
			<table class="table table-striped table-bordered table-hover" id="frogs_subpanel">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Gender</th>
						<th>Birth Date</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.table-responsive -->
	</div>
	<!-- /.panel-body -->
</div>
<script>
$(document).ready(function() {
	$('#frogs_subpanel').DataTable({
		"responsive": true,
		"processing": true,
        "serverSide": true,
		"ajax": {
            "url": "<?php echo base_url("frog/getlist/".$detail['id']);?>",
            "type": "POST",
			"dataSrc": function ( json ) {
				for ( var i=0, ien=json.data.length ; i<ien ; i++ ) {
					json.data[i]['first_name'] = '<a href="<?php echo base_url("frog/detail")?>/'+json.data[i]['id']+'">'+json.data[i]['first_name']+'</a>';
				}
				return json.data;
			}
        },
		"columns": [
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "gender" },
            { "data": "birth_date" }
        ],
		"columnDefs": [{
			"targets": [ 2 ],
			"orderable": false
		}]
	});
});


</script>