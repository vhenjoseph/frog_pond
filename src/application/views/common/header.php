<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Frog Pond</title>

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-datepicker.min.css");?>" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url("assets/css/metisMenu.min.css");?>" rel="stylesheet">
	<!-- DataTables CSS -->
    <link href="<?php echo base_url("assets/css/dataTables.bootstrap.css");?>" rel="stylesheet">
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url("assets/css/responsive.dataTables.min.css");?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url("assets/css/sb-admin-2.css");?>" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url("assets/css/morris.css");?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url("assets/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">
	
	<!-- jQuery -->
    <script src="<?php echo base_url("assets/js/jquery.min.js");?>"></script>
    <!-- Bootstrap JavaScript -->
    <script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap-datepicker.js");?>"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url("assets/js/metisMenu.min.js");?>"></script>
	<!-- DataTables JavaScript -->
    <script src="<?php echo base_url("assets/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/dataTables.bootstrap.min.js");?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url("assets/js/sb-admin-2.js");?>"></script>
</head>
<body>