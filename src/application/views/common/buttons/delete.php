<!-- Button trigger modal -->
	<button type="submit" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal">
		<i class="fa fa-trash-o fa-fw"></i> Delete
	</button>
	<!-- Modal -->
	<div class="modal fade" id="deleteModal" tabindex="1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Deleting</h4>
				</div>
				<div class="modal-body">
					Are you sure you want to delete this item?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button id = "submitDelete" type="button" class="btn btn-primary">OK</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<!-- /.modal -->
<script>
$(document).ready(function() {
	$("#submitDelete").click(function(){
        location.href = "<?php echo base_url($delete_link);?>";
    });
});

</script>