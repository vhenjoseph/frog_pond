<button id="edit" type="button" class="btn btn-info btn-sm"><i class="fa fa-pencil fa-fw"></i> Edit</button>
<script>
$(document).ready(function() {
	$("#edit").click(function() {
		location.href = "<?php echo base_url($edit_link);?>";
    });
});
</script>