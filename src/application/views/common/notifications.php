<!-- Notifications -->
<?php 
	$info = $this->session->flashdata("info");
	$success = $this->session->flashdata("success");
	$error = $this->session->flashdata("error");
	$warning = $this->session->flashdata("warning");
	
	if($success) {
		?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-check fa-fw"></i>
			<?php echo $success;?>
		</div>
		<?php
	}
	
	if($info) {
		?>
		<div class="alert alert-info alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-info fa-fw"></i>
			<?php echo $info;?>
		</div>
		<?php
	}
	
	if($error) {
		?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-times-circle fa-fw"></i>
			<?php echo $error;?>
		</div>
		<?php
	}
	
	if($warning) {
		?>
		<div class="alert alert-warning alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="fa fa-warning fa-fw"></i>
			<?php echo $warning;?>
		</div>
		<?php
	}
?>

	