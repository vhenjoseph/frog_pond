<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li>
				<a href="<?php echo base_url("dashboard");?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
			</li>
			<li>
				<a href="<?php echo base_url("pond/listall");?>"><i class="fa fa-cloud fa-fw"></i> Ponds <!-- <span class="fa arrow"></span> --></a>
				<!-- <ul class="nav nav-second-level">
					<li>
						<a href="<?php echo base_url("pond/listall");?>"><i class="fa fa-table fa-fw"></i> List</a>
					</li>
					<li>
						<a href="<?php echo base_url("pond/add");?>"><i class="fa fa-plus fa-fw"></i> Add Pond</a>
					</li>
				</ul> -->
			</li>
			<li>
				<a href="<?php echo base_url("frog");?>"><i class="fa fa-user fa-fw"></i> Frogs<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li>
						<a href="<?php echo base_url("frog/listall");?>"><i class="fa fa-table fa-fw"></i> List</a>
					</li>
					<li>
						<a href="<?php echo base_url("frog/add");?>"><i class="fa fa-plus fa-fw"></i> Add Frog</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>