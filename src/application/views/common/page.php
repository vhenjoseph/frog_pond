<?php $this->load->view("common/header");?>
    <div id="wrapper">
		<?php $this->load->view("common/navigation");?>
		<?php $this->load->view($page_to_use);?>
    </div>
<?php $this->load->view("common/footer");?>