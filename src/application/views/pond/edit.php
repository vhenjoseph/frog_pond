<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $detail['name'] ?> Pond</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" id="form_edit" method="post" action="<?php echo base_url("pond/process");?>">
						<div class="form-group">
							<input type="hidden" name="id" value="<?php echo $detail['id'];?>"/>
							<label>Pond Name <span class="text-danger">*</span></label>
							<input required="required" class="form-control" placeholder="Pond Name" name="name" value="<?php echo html_escape($detail['name']);?>"/>
							<?php echo form_error('name'); ?>
							<br/>
							<label>Age (weeks) <span class="text-danger">*</span></label>
							<input id="week_from" type="number" min="0" step="1" required="required" class="form-control" placeholder="Week From" name="week_from" value="<?php echo html_escape($detail['week_from']);?>"/>
							<input id="week_to" type="number" min="0" step="1" required="required" class="form-control" placeholder="Week To" name="week_to" value="<?php echo html_escape($detail['week_to']);?>"/>
							<?php echo form_error('week_from'); ?>
							<?php echo form_error('week_to'); ?>
							<br/>
							<label>Description</label>
							<textarea class="form-control" placeholder="Describe your pond here." name="description" maxlength="500"><?php echo html_escape($detail['description']);?></textarea>
							<?php echo form_error('description'); ?>
							<br/>
						</div>
						<?php $this->load->view("common/buttons/submit");?>
						<?php $this->load->view("common/buttons/cancel");?>
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>
