<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Ponds</h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Pond List
				</div>
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="ponds_list">
							<thead>
								<tr>
									<th>Name</th>
									<th>Age (weeks)</th>
									<th>Description</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#ponds_list').DataTable({
		"responsive": true,
		"processing": true,
        "serverSide": true,
		"stateSave": true,
		"ajax": {
            "url": "<?php echo base_url("pond/getlist");?>",
            "type": "POST",
			"dataSrc": function ( json ) {
				for ( var i=0, ien=json.data.length ; i<ien ; i++ ) {
					json.data[i]['name'] = '<a href="<?php echo base_url("pond/detail")?>/'+json.data[i]['id']+'">'+json.data[i]['name']+'</a>';
				}
				return json.data;
			}
        },
		"columns": [
            { "data": "name" },
            { "data": "age" },
            { "data": "description" }
        ],
		"columnDefs": [{
			"targets": [ 1,2 ],
			"orderable": false
		}]
	});
});
</script>