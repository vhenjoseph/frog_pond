<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $detail['name'] ?> Pond</h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php $this->load->view("common/buttons/edit", array("edit_link" => "pond/edit/".$detail['id']));?>
					<?php // $this->load->view("common/buttons/delete", array("delete_link" => "pond/delete/".$detail['id']));?>
				</div>
				<div class="panel-body">
					<dl class="dl-horizontal">
						<dt>Pond Name</dt>
						<dd><?php echo $detail['name']?></dd>

						<dt>Age (weeks)</dt>
						<dd><?php echo $detail['age']?></dd>
						
						<dt>Description</dt>
						<dd><?php echo $detail['description']?></dd>
					</dl>
				</div>
			</div>
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12">
			<?php $this->load->view("frog/subpanellist", array("detail" => $detail) );?>
		</div>
	</div>
</div>
