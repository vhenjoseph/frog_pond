<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Frog Relationship</h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
	<?php 
		$width = 10/count($detail);
			
		foreach( $detail as $key => $frog ) {
			$panel = "default";
			switch($frog['gender']) {
				case 'Male':
					$panel = 'primary';
					break;
				case 'Female':
					$panel = 'red';
					break;
			}
			
			if($key>0) {
				?>
				<div class="col-md-1">
					<h1 class="text-center text-danger"><i class="fa fa-heart fa-fw"></i></h1>
				</div>
				<?php
			}
			
			?>
			<div class="col-lg-<?php echo $width; ?>">
				<div class="panel panel-<?php echo $panel;?>">
					<div class="panel-heading text-center text-uppercase">
						<?php 
							echo $frog['first_name'].' '.$frog['last_name'];
						?>
					</div>
					<div class="panel-body">
						<dl class="dl-horizontal">
							<dt>First Name</dt>
							<dd><a href="<?php echo base_url("frog/detail/".$frog['id']);?>"><?php echo $frog['first_name']?></a></dd>

							<dt>Last Name</dt>
							<dd><?php echo $frog['last_name']?></dd>
							
							<dt>Gender</dt>
							<dd><?php echo $frog['gender']?></dd>
							
							<dt>Birth Date</dt>
							<dd><?php echo $frog['birth_date']?></dd>
						</dl>
					</div>
				</div>
			</div>
			<?php
		}
	?>
	</div>
</div>
