<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div>
	<?php $this->load->view("common/notifications");?>
	<div class="row">
		<?php $this->load->view("dashboard/frog_spawn", array("frog_spawn_count" => $frog_spawn_count) );?>
		<?php $this->load->view("dashboard/tadpoles", array("tadpoles_count" => $tadpoles_count));?>
		<?php $this->load->view("dashboard/frogs", array("frogs_count" => $frogs_count));?>
		<?php $this->load->view("dashboard/dead", array("zombies_count" => $zombies_count));?>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<?php $this->load->view("dashboard/birth_death_chart", array("birth_death_summary", $birth_death_summary));?>
		</div>
	</div>
</div>