<div class="panel panel-default">
	<div class="panel-heading">
		<i class="fa fa-bar-chart-o fa-fw"></i> Birth and Death Chart
		<div class="pull-right"></div>
	</div>
	<div class="panel-body">
		<div id="birth-death-chart"></div>
	</div>
</div>
<script>
$(document).ready(function() {
    Morris.Area({
        element: 'birth-death-chart',
        data: <?php echo $birth_death_summary?>,
        xkey: 'year',
        ykeys: ['birth_count', 'death_count'],
        labels: ['Birth Count', 'Death Count'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
});
</script>