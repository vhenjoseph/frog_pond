<div class="col-lg-3 col-md-6">
	<div class="panel panel-red">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-3">
					<i class="fa fa-support fa-5x"></i>
				</div>
				<div class="col-xs-9 text-right">
					<div class="huge"><?php echo number_format($zombies_count);?></div>
					<div>Zombies</div>
				</div>
			</div>
		</div>
		<a href="<?php echo base_url("pond/detail/4");?>">
			<div class="panel-footer">
				<span class="pull-left">View List</span>
				<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
</div>