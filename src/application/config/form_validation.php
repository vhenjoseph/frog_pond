<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();

$config = array(
	'edit_post' => array(
		array(
				'field' => 'name',
				'label' => 'Pond Name',
				'rules' => 'trim|strip_tags|required'
			 ),
		array(
				'field' => 'week_from',
				'label' => 'Week From',
				'rules' => 'trim|strip_tags|required|numeric'
			 ),
		array(
				'field' => 'week_to',
				'label' => 'Week To',
				'rules' => 'trim|strip_tags|required|numeric|greater_than['.$CI->input->post('week_from').']'
			 ),
		array(
				'field' => 'description',
				'label' => 'Description',
				'rules' => 'trim|strip_tags|max_length[500]'
			 )
	)
	,'edit_frog' => array(
		array(
				'field' => 'first_name',
				'label' => 'First Name',
				'rules' => 'trim|strip_tags|required|max_length[16]'
			 ),
		array(
				'field' => 'last_name',
				'label' => 'Last Name',
				'rules' => 'trim|strip_tags|required|max_length[16]'
			 ),
		array(
				'field' => 'birth_date',
				'label' => 'Birth Date',
				'rules' => 'trim|strip_tags|required|callback__valid_date'
			 ),
		array(
				'field' => 'gender',
				'label' => 'Gender',
				'rules' => 'trim|strip_tags|required|enum[M,F]'
			 )
	),
);

/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */
