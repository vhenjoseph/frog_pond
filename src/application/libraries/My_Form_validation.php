<?php
class MY_Form_validation extends CI_Form_validation {

    public function __construct($rules = array())
    {
        parent::__construct($rules);

        $this->_error_prefix = '<div class="text-danger pull-right">';
        $this->_error_suffix = '</div><br class="clear:float"/>';
    }
	
	function enum($str, $val='')
    {
		$this->set_message('enum', 'Please select a valid option.');
		
        if (empty($val))
			return FALSE;

        $arr = explode(',', $val);
        $array = array();
        foreach($arr as $value){
			$array[] = trim($value);
        }

        return (in_array(trim($str), $array)) ? TRUE : FALSE;
    }
}

/* End of file My_Form_validation.php */
/* Location: ./application/libraries/My_Form_validation.php */
