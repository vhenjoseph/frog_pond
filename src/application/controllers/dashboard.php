<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model("frogmodel", "frog");
	}
	
	function index() {
		$this->listall();
	}
	
	function listall(){
		
		$frog_spawn_count = $this->frog->get_list_count(false,1);
		$tadpole_count = $this->frog->get_list_count(false,2);
		$frogs_count = $this->frog->get_list_count(false,3);
		$zombies_count = $this->frog->get_list_count(false,4,true);
		
		$data['frog_spawn_count'] = $frog_spawn_count;
		$data['tadpoles_count'] = $tadpole_count;
		$data['frogs_count'] = $frogs_count;
		$data['zombies_count'] = $zombies_count;
		
		$birth_death_summary = $this->_get_bd_summary();
		$birth_death_summary = json_encode($birth_death_summary);
		$data['birth_death_summary'] = $birth_death_summary;
		
		$data['page_to_use'] = "dashboard/home";
		$this->load->view("common/page",$data);
	}
	
	function _get_bd_summary() {
		$birth_death_info = array();
		$return = array();
		
		$birth_info = $this->frog->get_birth_summary();
		$death_info = $this->frog->get_death_summary();
		
		foreach($birth_info as $binfo) {
			$birth_death_info[$binfo['year']]['year'] = $binfo['year'];
			$birth_death_info[$binfo['year']]['birth_count'] = $binfo['count'];
			if(!isset($birth_death_info[$binfo['year']]['death_count'])) {
				$birth_death_info[$binfo['year']]['death_count'] = 0;
			}
		}
		
		foreach($death_info as $dinfo) {
			$birth_death_info[$dinfo['year']]['year'] = $dinfo['year'];
			$birth_death_info[$dinfo['year']]['death_count'] = $dinfo['count'];
			if(!isset($birth_death_info[$binfo['year']]['birth_count'])) {
				$birth_death_info[$binfo['year']]['birth_count'] = 0;
			}
		}
		
		foreach($birth_death_info as $info) {
			$return[] = $info;
		}
		
		return $return;
	}
}

/* End of file frog.php */
/* Location: ./application/controllers/frog.php */