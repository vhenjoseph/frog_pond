<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frog extends CI_Controller {
	var $gender = array(
		"M" => "Male"
		,"F" => "Female"
	);
	
	var $pond_list = array();
	
	function __construct() {
		parent::__construct();
		
		// load model for retrieving frog info
		$this->load->model("Frogmodel", "frog");
		
		$this->load->model("Pondmodel", "pond");
		
		// get pond list from db
		$tmp_pond_list = $this->pond->get_list();
		foreach($tmp_pond_list as $pond) {
			$this->pond_list[$pond['id']] = $pond['name'];
		}
	}
	
	function index() {
		$this->listall();
	}
	
	function listall(){
		$data['page_to_use'] = "frog/list";
		$this->load->view("common/page",$data);
	}
	
	function edit($id){
		if($id == null) 
			redirect("pond/listall");
		
		$detail = $this->frog->get_detail($id);

		if(!$detail) {
			$this->session->set_flashdata('error', 'Frog was not found.');
			redirect("frog/listall");
		}
		
		// format birth date
		$detail['birth_date'] = date("m/d/Y", strtotime($detail['birth_date']));
		
		$data['detail'] = $detail;
		$data['pond_list'] = $this->pond_list;
		$data['page_to_use'] = "frog/edit";
		$this->load->view("common/page",$data);
	}
	
	function detail($id){
		if($id == null) 
			redirect("pond/listall");
		
		$detail = $this->frog->get_detail($id, true);
		
		if(!$detail) {
			$this->session->set_flashdata('error', 'Frog was not found.');
			redirect("frog/listall");
		}
		
		// format birth date
		$detail['birth_date'] = date("m/d/Y", strtotime($detail['birth_date']));
		$detail['death_date'] = date("m/d/Y", strtotime($detail['deleted_date']));
		$detail['gender'] = $this->gender[ $detail['gender'] ];
		$detail['pond'] = $this->pond_list[ $detail['pond_id'] ];
		
		// get mate info if exists
		$mate = $this->_get_mate_info($detail['id']);
				
		$data['detail'] = $detail;
		$data['mate_info'] = $mate;
		$data['page_to_use'] = "frog/detail";
		$this->load->view("common/page",$data);
	}
	
	function add(){
		$data['detail'] = null;
		$data['pond_list'] = $this->pond_list;
		$data['page_to_use'] = "frog/edit";
		$this->load->view("common/page",$data);
	}
	
	function delete($id){
		$detail = $this->frog->get_detail($id);
		
		if(!empty($detail['id'])) {
			if( !$this->_delete($id) ) {
				redirect("frog/detail/".$id);
			}
		}
		
		redirect("frog/listall");
	}
	
	function process(){
		// passing post values with xss filters
		$post = $this->input->post(NULL, TRUE);
		
		if( isset($post['submit']) ){ // if submit button is clicked			
			// Verify the data if it meets the criteria
			if ($this->form_validation->run('edit_frog') == FALSE) {
				$data['detail'] = $post;
				$data['page_to_use'] = "frog/edit";
				$this->load->view("common/page",$data);
			} else {
				// format date
				$post['birth_date'] = date("Y-m-d", strtotime($post['birth_date']));
				
				// for new frog, generate id
				if(empty($post['id'])){
					$post['id'] = $this->_add($post);
				} else {
					$this->_update($post);
				}
				
				redirect("frog/detail/".$post['id']);
			}
		} else if( isset($post['cancel']) ) {
			redirect("frog/detail/".$post['id']);
		}
	}
	
	function _add($post){
		$return = false;
		if( $return = $this->frog->save($post) ) {
			$this->session->set_flashdata('success', 'Frog has been added.');
		} else { // if adding did not work do the following
			$this->session->set_flashdata('error', 'Frog was not added.');
		}
		return $return;
	}
	
	function _update($post){
		if( $this->frog->save($post) ) {
			$this->session->set_flashdata('success', 'Frog has successfully been updated.');
		} else { // if updating did not work do the following
			$this->session->set_flashdata('error', 'frog was not updated.');
		}
	}
	
	function _delete($id){
		$return = false;
		if( $return = $this->frog->delete($id) ) {
			$this->session->set_flashdata('success', 'Frog has been deleted.');
		} else { // if updating did not work do the following
			$this->session->set_flashdata('error', 'Frog was not deleted.');
		}
		
		return $return;
	}
	
	function _get_mate_info($id) {
		$this->load->model("Matemodel", "mate");
		$mate_id = $this->mate->get_mate_id($id);
		
		return $this->frog->get_detail($mate_id, true);
	}
	
	function _valid_date($date){
		$tmp_date = array(null,null,null);
		$tmp_date = explode("/", $date);
		$this->form_validation->set_message('_valid_date', 'The %s is not a valid date');
		
		return checkdate((int)$tmp_date[0], (int)$tmp_date[1], (int)$tmp_date[2]);
	}
	
	function getlist($pond_id=false, $show_deleted=false){
		// datatables data
		$draw = $this->input->post("draw");
		$start = $this->input->post("start");
		$length = $this->input->post("length");
		$order = $this->input->post("order");
		$search = $this->input->post("search");
		$search = !empty($search['value']) ? $search['value'] : false;

		if($pond_id == 4) {
			$show_deleted = true;
		}
		
		// get list
		$frogs = $this->frog->get_list($search, $order[0], $length, $start, $pond_id, $show_deleted);
		
		foreach($frogs as $i => $frog) {
			$frogs[$i]['birth_date'] = date("m/d/Y", strtotime($frog['birth_date']));
			$frogs[$i]['death_date'] = date("m/d/Y", strtotime($frog['deleted_date']));
			$frogs[$i]['gender'] = $this->gender[ $frog['gender'] ];
		}
		
		// get total list count
		$frogs_count = $this->frog->get_list_count($search, $pond_id, $show_deleted);
		
		// populate data for json return
		$data = array(
			"draw" => $draw++
			,"recordsTotal" => $frogs_count
			,"recordsFiltered" => $frogs_count
			,"data" => $frogs
		);
		
		echo json_encode($data);		
	}
	
	function get_potential_mates($frog_id=false){
		// datatables data
		$draw = $this->input->post("draw");
		$start = $this->input->post("start");
		$length = $this->input->post("length");
		$order = $this->input->post("order");
		$search = $this->input->post("search");
		$search = !empty($search['value']) ? $search['value'] : false;
		
		$detail = $this->frog->get_detail($frog_id);
		
		if(!$detail) {
			$this->session->set_flashdata('error', 'Frog was not found.');
			redirect("frog/listall");
		}
		
		// get list
		$frogs = $this->frog->get_potential_mates($frog_id, $detail['gender'], $search, $order[0], $length, $start);
		
		foreach($frogs as $i => $frog) {
			$frogs[$i]['birth_date'] = date("m/d/Y", strtotime($frog['birth_date']));
			$frogs[$i]['gender'] = $this->gender[ $frog['gender'] ];
		}
		
		// get total list count
		$frogs_count = $this->frog->get_potential_mates_count($frog_id, $detail['gender'], $search);
		
		// populate data for json return
		$data = array(
			"draw" => $draw++
			,"recordsTotal" => $frogs_count
			,"recordsFiltered" => $frogs_count
			,"data" => $frogs
		);
		
		echo json_encode($data);		
	}
}

/* End of file frog.php */
/* Location: ./application/controllers/frog.php */