<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pond extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// load model for retrieving pond info
		$this->load->model("Pondmodel", "pond");
	}
	
	function index() {
		$this->listall();
	}
	
	function listall(){
		$data['page_to_use'] = "pond/list";
		$this->load->view("common/page",$data);
	}
	
	function detail($id){
		if($id == null) 
			redirect("pond/listall");
		
		$detail = $this->pond->get_detail($id);
		
		if(!$detail) {
			$this->session->set_flashdata('error', 'Pond was not found.');
			redirect("pond/listall");
		}
		
		$detail['age'] = $detail['week_from'].' - '.$detail['week_to'];

		$data['page_to_use'] = "pond/detail";
		
		if($id == 4) {
			$data['page_to_use'] = "pond/detail_deadpond";
		}
		
		$data['detail'] = $detail;
		$this->load->view("common/page",$data);
	}
	
	function edit($id){
		if($id == null) 
			redirect("pond/listall");
		
		$detail = $this->pond->get_detail($id);
		
		$data['detail'] = $detail;
		$data['page_to_use'] = "pond/edit";
		$this->load->view("common/page",$data);
	}
	
	function add(){
		$data['detail'] = null;
		$data['page_to_use'] = "pond/edit";
		$this->load->view("common/page",$data);
	}
	
	function delete($id){		
		$detail = $this->pond->get_detail($id);
		
		if(!empty($detail['id'])) {
			if( !$this->_delete($id) ) {
				redirect("pond/detail/".$id);
			}
		}
		
		redirect("pond/listall");
	}
	
	function process(){
		// passing post values with xss filters
		$post = $this->input->post(NULL, TRUE);
		
		if( isset($post['submit']) ){ // if submit button is clicked			
			// Verify the data if it meets the criteria
			if ($this->form_validation->run('edit_post') == FALSE) {
				$data['detail'] = $post;
				$data['page_to_use'] = "pond/edit";
				$this->load->view("common/page",$data);
			} else {
				// for new pond, generate id
				if(empty($post['id'])){
					$post['id'] = $this->_add($post);
				} else {
					$this->_update($post);
				}
				
				redirect("pond/detail/".$post['id']);
			}
		} else if( isset($post['cancel']) ) {
			redirect("pond/detail/".$post['id']);
		}
	}
	
	function _add($post){
		$return = false;
		if( $return = $this->pond->save($post) ) {
			$this->session->set_flashdata('success', 'Pond has been added.');
		} else { // if adding did not work do the following
			$this->session->set_flashdata('error', 'Pond was not added.');
		}
		return $return;
	}
	
	function _update($post){
		if( $this->pond->save($post) ) {
			$this->session->set_flashdata('success', 'Pond has successfully been updated.');
		} else { // if updating did not work do the following
			$this->session->set_flashdata('error', 'Pond was not updated.');
		}
	}
	
	function _delete($id){
		$return = false;
		if( $return = $this->pond->delete($id) ) {
			$this->session->set_flashdata('success', 'Pond has been deleted.');
		} else { // if updating did not work do the following
			$this->session->set_flashdata('error', 'Pond was not deleted.');
		}
		
		return $return;
	}
	
	function getlist(){
		// datatables data
		$draw = $this->input->post("draw");
		$start = $this->input->post("start");
		$length = $this->input->post("length");
		$order = $this->input->post("order");
		$search = $this->input->post("search");
		$search = !empty($search['value']) ? $search['value'] : false;
		
		// get list
		$ponds = $this->pond->get_list($search, $order[0], $length, $start);
		foreach($ponds as $key => $pond ) {
			$ponds[$key]['age'] = $pond['week_from'].' - '.$pond['week_to'];
		}
		
		// get total list count
		$ponds_count = $this->pond->get_list_count($search);
		
		// populate data for json return
		$data = array(
			"draw" => $draw++
			,"recordsTotal" => $ponds_count
			,"recordsFiltered" => $ponds_count
			,"data" => $ponds
		);
		
		// return data in json format
		echo json_encode($data);		
	}
}

/* End of file pond.php */
/* Location: ./application/controllers/pond.php */