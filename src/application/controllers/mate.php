<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mate extends CI_Controller {

	var $gender = array(
		"M" => "Male"
		,"F" => "Female"
	);

	function __construct() {
		parent::__construct();
		
		// load model for retrieving relationship info
		$this->load->model("Matemodel", "mate");
		$this->load->model("Frogmodel", "frog");
	}
	
	function detail($id){
		if($id == null) 
			redirect("mate/listall");
		
		$detail = $this->mate->get_detail($id);
		
		if(!$detail) {
			$this->session->set_flashdata('error', 'Relationship was not found.');
			redirect("mate/listall");
		}
		
		$frog_1 = $this->frog->get_detail($detail['frog_id1']);
		$frog_2 = $this->frog->get_detail($detail['frog_id2']);
		
		if(!$frog_1 || !$frog_2) {
			$this->session->set_flashdata('error', 'One of the frogs was not found.');
			redirect("frog/listall/");
		}
		
		// format birth date and gender
		$frog_1['birth_date'] = date("m/d/Y", strtotime($frog_1['birth_date']));
		$frog_2['birth_date'] = date("m/d/Y", strtotime($frog_2['birth_date']));
		$frog_1['gender'] = $this->gender[ $frog_1['gender'] ];
		$frog_2['gender'] = $this->gender[ $frog_2['gender'] ];
		
		$data['detail'][] = $frog_1;
		$data['detail'][] = $frog_2;
		$data['page_to_use'] = "mate/detail";
		$this->load->view("common/page",$data);
	}
	
	function add($frog_id1, $frog_id2){
		$mate_id = false;
		$frog_id1 = $this->db->escape_str($frog_id1);
		$frog_id2 = $this->db->escape_str($frog_id2);
		
		$frog_detail1 = $this->frog->get_detail($frog_id1);
		$frog_detail2 = $this->frog->get_detail($frog_id2);
		
		if(!$frog_detail1 || !$frog_detail2) {
			$this->session->set_flashdata('error', 'One of the frogs was not found.');
			redirect("frog/listall/");
		}
		
		if( $mate_id = $this->_add($frog_detail1['id'], $frog_detail2['id']) ) {
			redirect("mate/detail/".$mate_id);
		} else {
			redirect("frog/listall/");
		}
	}
	
	function _add($frog_id1, $frog_id2){
		$return = false;
		if( $return = $this->mate->add($frog_id1, $frog_id2) ) {
			$this->session->set_flashdata('success', 'Both frogs are a match!');
		} else { // if adding did not work do the following
			$this->session->set_flashdata('error', 'There\'s something wrong. We think both frogs are not compatible.');
		}
		return $return;
	}
}

/* End of file pond.php */
/* Location: ./application/controllers/pond.php */