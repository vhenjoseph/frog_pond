<?php
class Pondmodel extends CI_Model {

    var $tablename = 'ponds';
	var $columns = array(
		// "id"
		"name"
		,"age"
		,"description"
	);

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_list($search=false, $order=false, $limit=false, $offset=false)
    {
		$this->db->select();
		
		if($search!==false) {
			$this->db->like("name", $search);
		}
		
		$this->db->from($this->tablename);
		
		if($limit!==false && $offset!==false) {
			$this->db->limit($limit, $offset);
        } else if($limit!==false){
			$this->db->limit($limit);
		}
		
		if($order!==false){
			$this->db->order_by($this->columns[$order['column']], $order['dir']);
		}
		
		$result = $this->db->get();

        return $result->result_array();
    }

	function get_list_count($search=false){
		$this->db->select();
		
		if($search!==false) {
			$this->db->like("name", $search);
		}
		
		$this->db->from($this->tablename);
		return $this->db->count_all_results();
    }
	
     function get_detail($id)
    {
		$id = $this->db->escape_str($id);
        $result = $this->db->get_where($this->tablename, array('id' => $id));
        $detail = $result->result_array();
		
        return isset($detail[0]) ? $detail[0] : false;
    }
	
	 function _generate_id()
    {
		$this->db->select_max('id');
		$result = $this->db->get($this->tablename);
        $detail = $result->result_array();
        return $detail[0]['id']+1;
    }

    function save($data) {
		$result = false;
		$values = array(
			"id" => $data['id']
			,"name" => $data['name']
			,"week_from" => $data['week_from']
			,"week_to" => $data['week_to']
			,"description" => $data['description']
		);
		
		if(!empty($data['id'])) {
			$this->db->where('id', $data['id']);
			$result = $this->db->update($this->tablename, $values);
		} else {
			$values['id'] = $this->_generate_id();
			$result = $this->db->insert($this->tablename, $values);
		}

		if($result)
			return $values['id'];
		
    }
	
	function delete($id) {
		return $this->db->delete($this->tablename, array('id' => $id)); 
    }


}
?>