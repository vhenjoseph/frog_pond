<?php
class Frogmodel extends CI_Model {

    var $tablename = 'frogs';
	var $columns = array(
		// "id"
		"first_name"
		,"last_name"
		,"gender"
		,"birth_date"
		,"deleted_date"
	);

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_list($search=false, $order=false, $limit=false, $offset=false, $pond_id=false, $show_deleted=false){
		$search = $this->db->escape_str($search);
		$this->db->select();

		$this->db->from($this->tablename);
		
		if($search!==false) {
			$this->db->where("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%')", null);
		}

		if($pond_id!==false) {
			$this->db->where("pond_id", $pond_id);
		}
		
		if($show_deleted == false) {
			$this->db->where("deleted", 0);
		}
		
		if($limit!==false && $offset!==false) {
			$this->db->limit($limit, $offset);
        } else if($limit!==false){
			$this->db->limit($limit);
		}
		
		if($order!==false){
			$this->db->order_by($this->columns[$order['column']], $order['dir']);
		}
		
		$result = $this->db->get();

        return $result->result_array();
    }
	
	function get_list_count($search=false, $pond_id=false, $show_deleted=false){
		$this->db->select();
		
		$this->db->from($this->tablename);
		
		if($search!==false) {
			$this->db->where("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%')", null);
		}
		
		if($pond_id!==false) {
			$this->db->where("pond_id", $pond_id);
		}
		
		if(!$show_deleted) {
			$this->db->where("deleted", 0);
		}
		
		return $this->db->count_all_results();
    }

	function get_potential_mates($frog_id, $gender, $search=false, $order=false, $limit=false, $offset=false, $show_deleted=false){
		$search = $this->db->escape_str($search);
		$this->db->select();

		$this->db->from($this->tablename);
		$this->db->where("pond_id", 3);
		$this->db->where("id !=", $frog_id);
		$this->db->where("gender !=", $gender);
		
		if($search!==false) {
			$this->db->where("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%')", null);
		}
		
		if($show_deleted == false) {
			$this->db->where("deleted", 0);
		}
		
		if($limit!==false && $offset!==false) {
			$this->db->limit($limit, $offset);
        } else if($limit!==false){
			$this->db->limit($limit);
		}
		
		if($order!==false){
			$this->db->order_by($this->columns[$order['column']], $order['dir']);
		}
		
		$result = $this->db->get();

        return $result->result_array();
    }
	
	function get_potential_mates_count($frog_id, $gender, $search=false, $show_deleted=false){
		$this->db->select();
		
		$this->db->from($this->tablename);
		$this->db->where("pond_id", 3);
		$this->db->where("id !=", $frog_id);
		$this->db->where("gender !=", $gender);
		
		if($search!==false) {
			$this->db->where("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%' OR CONCAT(first_name, ' ', last_name) LIKE '%{$search}%')", null);
		}

		if(!$show_deleted) {
			$this->db->where("deleted", 0);
		}
		
		return $this->db->count_all_results();
    }
	
	function get_detail($id, $show_deleted=false)
    {
		$id = $this->db->escape_str($id);
		$values = array(
			'id' => $id
		);
		
		if($show_deleted == false) {
			$values['deleted'] = 0;
		}
		
        $result = $this->db->get_where($this->tablename, $values);
        $detail = $result->result_array();
		
        return isset($detail[0]) ? $detail[0] : false;
    }
    
	 function _generate_id()
    {
		$this->db->select_max('id');
		$result = $this->db->get($this->tablename);
        $detail = $result->result_array();
        return $detail[0]['id']+1;
    }
	
	function save($data) {
		$result = false;
		$values = array(
			"id" => $data['id']
			,"first_name" => $data['first_name']
			,"last_name" => $data['last_name']
			,"birth_date" => $data['birth_date']
			,"gender" => $data['gender']
			,"pond_id" => $data['pond_list']
		);
		
		if(!empty($data['id'])) {
			$this->db->where('id', $data['id']);
			$result = $this->db->update($this->tablename, $values);
		} else {
			$values['id'] = $this->_generate_id();
			$result = $this->db->insert($this->tablename, $values);
		}

		if($result)
			return $values['id'];
		
    }
	
	function delete($id) {
		$result = false;
		$values = array(
			"deleted" => 1
			,"deleted_date" => date("Y-m-d")
			,"pond_id" => 4
		);
		
		$this->db->where('id', $id);
		
		$result = $this->db->update($this->tablename, $values);
		return $result; 
    }

	function get_birth_summary() {
		$result = false;
		
		$select = array(
			"YEAR(birth_date) AS year"
			,"COUNT(birth_date) AS count"
		);
		
		$this->db->select($select);
		$this->db->group_by('YEAR(birth_date)');
		
		$result = $this->db->get($this->tablename);
		
		return $result->result_array(); 
	}
	
	function get_death_summary() {
		$result = false;
		
		$select = array(
			"YEAR(deleted_date) AS year"
			,"COUNT(deleted_date) AS count"
		);
		
		$this->db->select($select);
		$this->db->group_by('YEAR(deleted_date)');
		$this->db->where('deleted_date IS NOT NULL', NULL);
		
		$result = $this->db->get($this->tablename);
		
		return $result->result_array(); 
	}
}
?>