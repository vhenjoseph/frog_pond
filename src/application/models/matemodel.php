<?php
class Matemodel extends CI_Model {

    var $tablename = 'mates';
	var $columns = array(
		// "id"
		"frog_id1"
		,"frog_id2"
	);

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function _generate_id()
    {
		$this->db->select_max('id');
		$result = $this->db->get($this->tablename);
        $detail = $result->result_array();
        return $detail[0]['id']+1;
    }
	
	function add($frog_1, $frog_2) {
		$result = false;
		$values = array(
			"id" => $this->_generate_id()
			,"frog_id1" => $frog_1
			,"frog_id2" => $frog_2
		);
		
		$result = $this->db->insert($this->tablename, $values);

		if($result)
			$result = $values['id'];
		
		return $result;
    }
	
	function get_detail($id)
    {
		$id = $this->db->escape_str($id);
		$values = array(
			'id' => $id
		);
		
        $result = $this->db->get_where($this->tablename, $values);
        $detail = $result->result_array();
		
        return isset($detail[0]) ? $detail[0] : false;
    }
	
	function get_mate_id($frog_id) {
		$this->db->select();
		$this->db->where("frog_id1", $frog_id);
		$this->db->or_where("frog_id2", $frog_id);
		$result = $this->db->get($this->tablename);
		
		$detail = $result->result_array();
		
		if($result->num_rows() > 0) {
			return ($frog_id == $detail[0]['frog_id1']) ? $detail[0]['frog_id2'] : $detail[0]['frog_id1'];
		} else {
			return false;
		}
		
	}
	
}
?>