<!---
Add your comments / notes and thoughts to this doc

Disclaimer: 
No frogs were harmed during the development and testing process.
A combination of Codeigniter, jQuery, Bootstrap, Datatables and Morris graph were used in a WAMP environment.
Also, this my first time working with Bootstrap, Datatables, and Morris so I hope I did prove something about being adaptive to a new technology.

This is FrogPond.

There are four stages of frog life cycle and so there are four types of ponds:

1. Frog Spawn 
 - where frog eggs live from birth until 3weeks.
 
2. Tadpoles
 - where tadpoles swim around until they become a full adult frog (usually takes 4-5 months)

3. Frogs
 - where adult frogs live.
 - this is the only place where they are paired with other frogs. Lot's of mosquitoes as well so please be careful.
   - How to pair?
      Just open a detail view of a frog and select a partner and become a matchmaker. :)

4. Zombie
 - where all dead spawns, tadpoles and frogs came back to the living and do nothing.

Enjoy!
 
P.S.
 Do keep in mind I am working in parallel with my current schedule for my current employer.
 So here is the list of my work time from start to end.
	4/18/2016	8pm - 9pm 
	4/19/2016	6pm - 8pm
	4/20/2016	130pm - 530pm
	4/20/2016	900am - 1130pm
	4/21/2016	8pm - 12pm
	4/22/2016	630pm - 800pm
	4/23/2016	10am - 830pm
	4/24/2016	8pm - 10pm
	4/24/2016	130pm - 530pm
	4/24/2016	8pm - 11pm
 

Any special instructions to running your code?
1. Apache settings 
  Enable mod_rewrite
   - in WAMP settings in app tray, go to Apache -> Apache modules -> check rewrite module
   - OR, in httpd.conf, remove "#" in "#LoadModule rewrite_module modules/mod_rewrite.so"

2. Database setup
   - Import database "src/frogpond.sql.rar"
   - In src/application/config/database.php, define database info

3. Codeigniter setup (the following instructions may already be implemented)
  Remove index.php in url
   - Go to "src/application/config/config.php" 
   - SET $config['index_page'] to '';
   - create src/.htaccess in root folder and write the following code
    	RewriteEngine on
		RewriteBase /frogasia_tech_test/src/
		RewriteCond $1 !^(index.php|resources|robots.txt)
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteRule ^(.*)$ index.php/$1 [L,QSA]

     Note: RewriteBase should be your root folder

  src/application/config/autoload.php
   - add libraries 'database','session', and 'form_validation'
   - add helpers 'url','form', and 'date'

  src/application/config/config.php
   - define base_url such as http://localhost/FrogPond
   - empty index_page
   - define encryption_key, this will be used for session; I used 'frogpondencryptionkey'
   - set global_xss_filtering to TRUE so that XSS filter will be active always when GET, POST, or Cookie data is encountered

  src/application/config/database.php
   - define the database info and import database frogpond.sql

  src/application/config/routes.php
   - set default_controller to dashboard

-->
